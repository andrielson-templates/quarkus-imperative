package com.gitlab.andrielson.quarkus_imperative;

import com.gitlab.andrielson.quarkus_imperative.user.User;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class GreetingResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        User user = new User();
        return "Hello RESTEasy";
    }
}