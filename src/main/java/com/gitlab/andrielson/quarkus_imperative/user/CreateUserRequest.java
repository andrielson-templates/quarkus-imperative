package com.gitlab.andrielson.quarkus_imperative.user;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Schema(name = "Novo usuário")
public record CreateUserRequest(
        @Schema(description = "Identificador do usuário", minLength = USERNAME_MIN_LENGTH, maxLength = USERNAME_MAX_LENGTH)
        @Length(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH)
        @NotBlank
        String username,
        @Schema(description = "Senha do usuário", minLength = PASSWORD_MIN_LENGTH, maxLength = PASSWORD_MAX_LENGTH)
        @Length(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
        @NotBlank
        String password,
        @Schema(description = "Papel do usuário", enumeration = {"admin", "user"})
        @NotBlank
        String role) {
    static final int USERNAME_MAX_LENGTH = 32;
    static final int USERNAME_MIN_LENGTH = 8;
    static final int PASSWORD_MAX_LENGTH = 32;
    static final int PASSWORD_MIN_LENGTH = 8;
}