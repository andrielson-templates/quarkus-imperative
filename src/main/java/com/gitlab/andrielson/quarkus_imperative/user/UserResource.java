package com.gitlab.andrielson.quarkus_imperative.user;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.Instant;

public record UserResource(
        @Schema(description = "ID do usuário")
        Long id,
        @Schema(description = "Identificador do usuário")
        String username,
        @Schema(description = "Papel do usuário", enumeration = {"admin", "user"})
        String role,
        @Schema(description = "Data e hora da criação do usuário", example = "2022-02-27T01:58:03.713579Z")
        Instant createdAt,
        @Schema(description = "Data e hora da última atualização do usuário", example = "2022-02-27T01:58:03.713579Z")
        Instant updatedAt) {
}
