package com.gitlab.andrielson.quarkus_imperative.user;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Tag(name = "Usuários")
@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserController {

    private final UserService userService;

    @Inject
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Cria um novo usuário", description = "Cria um novo usuário informando senha e papel")
    @POST
    public Response create(@Valid CreateUserRequest createUserRequest) {
        var createUserDto = new CreateUserDto(createUserRequest.username(), createUserRequest.password(), createUserRequest.role());
        userService.create(createUserDto);
        return Response.status(Response.Status.CREATED).build();
    }

    @Operation(summary = "Lista os usuários", description = "Lista todos os usuários cadastrados no sistema")
    @APIResponse(description = "Lista dos usuários",
            content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = UserResource.class, type = SchemaType.ARRAY)
            )
    )
    @GET
    public Response list() {
        var resourceStream = userService.findAll().map(this::mapUserToUserResource);
        return Response.ok(resourceStream).build();
    }

    private UserResource mapUserToUserResource(final User user) {
        return new UserResource(
                user.id,
                user.username,
                user.role,
                user.createdAt,
                user.updatedAt);
    }
}
