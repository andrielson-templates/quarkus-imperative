package com.gitlab.andrielson.quarkus_imperative.user.resources;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.validation.Valid;
import javax.ws.rs.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Tag(name = "Usuários")
@Path("/users")
public class UpdateUserResource extends UserResourcesBase {

    @PATCH
    public Response update(@Valid UpdateUserRequest updateUserRequest) {
        return Response.noContent().build();
    }
}
