package com.gitlab.andrielson.quarkus_imperative.user.resources;

import com.gitlab.andrielson.quarkus_imperative.user.UserService;

import javax.inject.Inject;

public abstract class UserResourcesBase {
    @Inject
    protected UserService userService;
}
