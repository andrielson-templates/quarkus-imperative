package com.gitlab.andrielson.quarkus_imperative.user;

import io.quarkus.elytron.security.common.BcryptUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.stream.Stream;

@ApplicationScoped
public class UserService {

    @Transactional
    public void create(@NotNull final CreateUserDto createUserDto) {
        var user = new User(createUserDto.name(), BcryptUtil.bcryptHash(createUserDto.password()), createUserDto.role());
        user.persistAndFlush();
    }

    public Stream<User> findAll() {
        return User.streamAll();
    }

}
