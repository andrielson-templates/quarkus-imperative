package com.gitlab.andrielson.quarkus_imperative.user;

public record CreateUserDto(String name, String password, String role) {
}
