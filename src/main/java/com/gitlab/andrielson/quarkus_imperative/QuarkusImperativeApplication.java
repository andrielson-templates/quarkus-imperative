package com.gitlab.andrielson.quarkus_imperative;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;

import javax.ws.rs.core.Application;


@OpenAPIDefinition(
        info = @Info(
                title="Quarkus Imperative Application",
                version = "1.0.0-SNAPSHOT",
                contact = @Contact(
                        name = "Andrielson Silva",
                        url = "https://gitlab.com/andrielson",
                        email = "andrielson@gmail.com"),
                license = @License(
                        name = "MIT License",
                        url = "https://gitlab.com/andrielson-templates/quarkus-imperative/-/raw/main/LICENSE"))
)
public class QuarkusImperativeApplication extends Application {
}
